import React,{Component} from 'react';
import {connect} from 'react-redux';
import {  Router, Route, Switch,Redirect } from 'react-router-dom';
import History from '../history_utils'
class Login extends Component{
	 constructor(props) {
    super(props);
  
    this.state = {errorEmail:''};
  }

  emailValidation=(email)=>{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))return true;
    return false;
  }

  loginEmail=(e)=>{
    if(this.emailValidation(e.target.value)){
     	this.setState({email:e.target.value,errorEmail:''})
    }
    else{
     this.setState({errorEmail:'enter valid email id'})
    }

  }

  loginPassword=(e)=>{
      this.setState({password:e.target.value})
  }


  login=()=>{
    if(this.state.email&&this.state.password){
    	this.props.login(this.state.email,this.state.password)
    }
  }

  componentDidUpdate=()=>{
    if(this.props.isAuthenticate){
      return History.push('/employee');
    }
  }

  render(){
  
  
    return (
       <React.Fragment>
         <input type="text" placeholder="Enter the username" onChange={(e)=>this.loginEmail(e)}/><br/>
         {this.state.errorEmail}<br/>
         <input type="password" placeholder="Enter the password" onChange={(e)=>this.loginPassword(e)}/><br/>
         <button onClick={this.login}>Log In</button>
         {this.props.loginError}
       </React.Fragment>
    );
  }
}
const mapStateToProps=(state)=>{
	return{
		users:state.user,
    isAuthenticate:state.isAuthenticate,
    loginError:state.loginError,
	}
}
const mapDispatchToProps=(dispatch)=>{
	return{
		login:(email,password)=>{
			dispatch({type:"LOGIN",payload:{email:email,password:password}})
		}
	}
}
export default connect(mapStateToProps,mapDispatchToProps)(Login)