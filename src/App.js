import React,{Component} from 'react';
import Reducer from './Reducer';
import {createStore}  from 'redux'
import {Provider} from 'react-redux'
import {  Router, Route, Switch } from 'react-router-dom';
import History from './history_utils'
import Routers from './Routers'
const store=createStore(Reducer); 


 function App () { 

 return (
   <React.Fragment>
     <Provider store={store}>
       <Router history={History}>
       {<Route path="/" render={props => (
                <Routers
                  {...props}

                />

              )} />}       
       </Router>
     </Provider>
   </React.Fragment>

   )
}

export default App;
