import React,{Component} from 'react';
import {connect} from 'react-redux';
import './styles.scss'
 class Employee extends Component{
 	constructor(props) {
 	  super(props);
 	  this.state = {};
 	}
 	getemployeeList=()=>{
 		return this.props.users.map(item=>{
 			return (<tr>
	 				<td>{item.id}</td>
	 				<td>{item.name}</td>
	 				<td>{item.age}</td>
	 				<td>{item.gender}</td>
	 				<td>{item.email}</td>
	 				<td>{item.phoneNo}</td>
 				</tr>)
 		})
 	}
	render(){
		return(
			<React.Fragment>
			<h3> Employee List</h3>
			<table>
				<tr>
					<th>id</th>
					<th>name</th>
					<th>age</th>
					<th>gender</th>
					<th>email</th>
					<th>phoneNo</th>
				</tr>
				{this.getemployeeList()}
			</table>
			</React.Fragment>

			)
	}
}
const mapStateToProps=(state)=>{
	return{
		users:state.user,
	}
}
const mapDispatchToProps=(dispatch)=>{
	return{
		
	}
}
export default connect(mapStateToProps,mapDispatchToProps)(Employee)