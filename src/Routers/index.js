import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import Login from '../Login'
import EmployeeList from '../EmployeeList'
export default function Routers(props){
	const {match}=props
	return (
		
		<Switch>
			<Route   path ={`${match.path}`} exact render={()=><Login/>}/>
          	<Route path ={`${match.path}employee`} render={()=><EmployeeList/>}/>
		</Switch>
		
	)
}